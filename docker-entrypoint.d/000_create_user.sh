#!/usr/bin/env bash

. /docker-entrypoint-common.sh

set -e

APPLICATION_USER=${APPLICATION_USER:="pathfinder"}
APPLICATION_UID=${APPLICATION_UID:="10000"}
APPLICATION_GROUP=${APPLICATION_GROUP:="pathfinder"}
APPLICATION_GID=${APPLICATION_GID:="10000"}
APPLICATION_CHOWN=${APPLICATION_CHOWN:="yes"}

if [ -z "$(getent group $APPLICATION_GROUP)" ]; then
	ngx_info "create app group: '$APPLICATION_GROUP'"
	groupadd "$APPLICATION_GROUP" -g "$APPLICATION_GID"
fi

if [ -z "$(getent passwd $APPLICATION_USER)" ]; then
	ngx_info "create app user: '$APPLICATION_USER'"
	useradd -M -s /bin/bash -g "$APPLICATION_GROUP" -u "$APPLICATION_UID" "$APPLICATION_USER"
fi

if [ ! -z "$APPLICATION_DIR" ]; then
	if [ ! -d "$APPLICATION_DIR" ]; then
		ngx_info "create app dir: '$APPLICATION_DIR'"
		mkdir -p "$APPLICATION_DIR"
	fi
	usermod --home "$APPLICATION_DIR" "$APPLICATION_USER" &>/dev/null
	if [ "x$APPLICATION_CHOWN" = "xyes" ]; then
		find "$APPLICATION_DIR" \! -user "$APPLICATION_USER" -exec chown "$APPLICATION_USER":"$APPLICATION_GROUP" '{}' +
	fi
fi
