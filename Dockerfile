FROM 6run0/nginx-unit-php:bookworm-1.34.1-1-php7.4

ARG DEBIAN_FRONTEND=noninteractive

# See https://github.com/aptible/supercronic/releases
ARG SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.2.33/supercronic-linux-amd64
ARG SUPERCRONIC=supercronic-linux-amd64
ARG SUPERCRONIC_SHA1SUM=71b0d58cc53f6bd72cf2f293e09e294b79c666d8

# See https://github.com/composer/composer
ARG COMPOSER_URL=https://github.com/composer/composer/releases/latest/download/composer.phar
ARG COMPOSER_HASH=https://github.com/composer/composer/releases/latest/download/composer.phar.asc
ARG COMPOSER_KEY=161DFBE342889F01DDAC4E61CBB3D576F2A0946F

# I don't know why yet!
# https://github.com/nodesource/distributions?tab=readme-ov-file#debian-versions
ARG NODE_SETUP=https://deb.nodesource.com/setup_21.x

RUN \
  set -eux; \
  savedAptMark="$(apt-mark showmanual)"; \
  apt update; \
  apt install -y --no-install-recommends ca-certificates dirmngr gnupg wget php-pear build-essential php7.4-dev; \
  apt-mark auto '.*' > /dev/null; \
  [ -z "$savedAptMark" ] || apt-mark manual $savedAptMark > /dev/null; \
  cd /usr/local/bin; \
  # install composer
  wget -O composer ${COMPOSER_URL}; \
  wget -O composer.asc ${COMPOSER_HASH}; \
  export GNUPGHOME="$(mktemp -d)"; \
  gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys ${COMPOSER_KEY}; \
  gpg --batch --verify composer.asc composer; \
  gpgconf --kill all; \
  rm -rf "$GNUPGHOME" composer.asc; \
  chmod +x composer; \
  # install supercronic
  wget -O "$SUPERCRONIC" "$SUPERCRONIC_URL"; \
  echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c -; \
  chmod +x "$SUPERCRONIC"; \
  mv "$SUPERCRONIC" /usr/local/bin/supercronic; \
  # installing the required packages
  apt install -y --no-install-recommends \
  curl \
  git \
  gosu \
  libevent-dev \
  logrotate \
  msmtp \
  openssh-client \
  unzip \
  ; \
  # install event https://pecl.php.net/package/event
  pecl install -n event; \
  echo "extension=event.so" > /etc/php/7.4/mods-available/event.ini; \
  ln -s /etc/php/7.4/mods-available/event.ini /etc/php/7.4/cli/conf.d/99-event.ini; \
  ln -s /etc/php/7.4/mods-available/event.ini /etc/php/7.4/embed/conf.d/99-event.ini; \
  # install nodejs
  curl -fsSL ${NODE_SETUP} | bash -; \
  apt install -y --no-install-recommends nodejs; \
  # use msmtp as sendmail
  ln -sf /usr/bin/msmtp /usr/sbin/sendmail; \
  # clean
  apt purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
  apt autoclean -y; \
  rm -rf /var/lib/apt/lists/*; \
  rm -rf /var/cache/apt/archives/*; \
  composer --version; \
  curl --version; \
  git --version; \
  msmtp --version; \
  ssh -V; \
  unzip -v; \
  gosu --version; \
  unitd --version

ENV APPLICATION_USER=pathfinder
ENV APPLICATION_UID=10000
ENV APPLICATION_GROUP=pathfinder
ENV APPLICATION_GID=10000
ENV APPLICATION_DIR=/pathfinder

RUN \
  set -eux; \
  groupadd -g $APPLICATION_GID $APPLICATION_GROUP; \
  useradd -c "Pathfinder user" -d $APPLICATION_DIR -s /bin/bash -g $APPLICATION_GROUP -u $APPLICATION_UID $APPLICATION_USER;

WORKDIR $APPLICATION_DIR

COPY . /
