# Base image for Pathfinder

Pathfinder is a mapping tool for [EVE ONLINE](https://www.eveonline.com/)

See:

- [exodus4d/pathfinder](https://github.com/exodus4d/pathfinder)
- [goryn-clade/pathfinder](https://github.com/goryn-clade/pathfinder)
- [jithatsonei/pathfinder](https://github.com/jithatsonei/pathfinder)

## Environment variables

| Variable          | Description                                                      | Default     |
| ----------------- | ---------------------------------------------------------------- | ----------- |
| APPLICATION_USER  | The name of the non-privileged user to run the application       | pathfinder  |
| APPLICATION_UID   | The ID of the non-privileged user to run the application         | 10000       |
| APPLICATION_GROUP | The group name of the non-privileged user to run the application | pathfinder  |
| APPLICATION_GID   | The group ID of the non-privileged user to run the application   | 10000       |
| APPLICATION_DIR   | The directory where the application is installed (WORKDIR)       | /pathfinder |
| APPLICATION_CHOWN | Set permissions for directory files when starting the container  | yes         |
